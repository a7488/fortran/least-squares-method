program mnk
    use mod1
    implicit none
    real, allocatable :: x(:), y(:), x1(:)
    real a, b, e
    integer n, i

    open(1, file = "input") 
    open(2, file = "result") 

    
    read(1, *) n 
    allocate(x(1:n), y(1:n))
    
    read(1, *) x
    write(2, *) "x(i):", x
    read(1, *) y
    write(2, *) "y(i):", y


    write(2, *) "______________________________________________________________________________________"

    call linear(x, y, a, b, n)
    write(2, *) "Линейная f(x) = ax + b"
    write(2, *) "Коэффициент а=",a ,"b=", b
    e = 0
    do i = 1, n
        e = e + (y(i) - pogr1(x(i), a, b)) ** 2
    enddo
    write(2, *) "Погрешность=", e
    write(2, *) "______________________________________________________________________________________"

    

    call hyper(x, y, a, b, n)
    write(2, *) "Гиперболическая f(x) = a/x + b"
    write(2, *) "Коэффициент а=",a ,"b=", b
    e = 0
    do i = 1, n
        e = e + (y(i) - pogr2(x(i), a, b)) ** 2
    enddo
    write(2, *) "Погрешность=", e
    write(2, *) "______________________________________________________________________________________"

   

    call logarifm(x, y, a, b, n)
    write(2, *) "Логарифмическая f(x) = a + b*ln(x)"
    write(2, *) "Коэффициент а=",a ,"b=", b
    e = 0
    do i = 1, n
        e = e + (y(i) - pogr3(x(i), a, b)) ** 2
    enddo
    write(2, *) "Погрешность=", e

    write(2, *) "______________________________________________________________________________________"

    call expan(x, y, a, b, n)
    write(2, *) "Экспоненциальная f(x) = a*exp(bx)"
    write(2, *) "Коэффициент а=",b ,"b=", a
    e = 0
    do i = 1, n
        e = e + (y(i) - pogr4(x(i), a, b)) ** 2
    enddo
    write(2, *) "Погрешность=", e
    write(2, *) "______________________________________________________________________________________"

    e = 0
    
    call quadra(x, y, x1, n)
    write(2, *) "Квадратичная f(x) = ax^2 + bx + c"
    write(2, *) "Коэффициент а=",x1(1) ,"b=", x1(2), "c=", x1(3)
    do i = 1, n
        e = e + (y(i) - pogr5(x(i), x1(1), x1(2), x1(3))) ** 2
    enddo
    write(2, *) "Погрешность=", e

end program mnk