module mod1
    implicit none
    contains

    subroutine linear(x, y, a, b, n)
        real :: x(:), y(:)
        real a, b
        integer n, i

        
        a = (n * sum(x*y) - sum(x)*sum(y))/(n * sum(x**2) - (sum(x))**2)
        b = (sum(y) - a*sum(x))/n
        
    end subroutine linear

    function pogr1(x, a, b)
        real a, b, pogr1, x
        pogr1 = a*x + b
    end function pogr1
        

    subroutine hyper(x, y, a, b, n)
        real, allocatable :: x(:), y(:)
        real a, b
        integer n
        
        a = (n * sum(y/x) - sum(1/x)*sum(y))/(n * sum(1/x**2) - (sum(1/x))**2)
        b = (sum(y) - a*sum(1/x))/n
 

    
    end subroutine hyper

    function pogr2(x, a, b)
        real a, b, pogr2, x
        pogr2 = a/x + b
    end function pogr2

    subroutine logarifm(x, y, a, b, n)
        real, allocatable :: x(:), y(:)
        real a,b
        integer n

        b = ( sum(y) * sum ((log(x))**2) - sum(y * log(x)) * sum(log(x)))/( n * sum((log(x))**2) - (sum(log(x)))**2) 
        a = (n * sum( y * log(x)) - sum(y)*sum(log(x)))/( n * sum((log(x))**2) - (sum(log(x)))**2) 
    
    end subroutine logarifm

    function pogr3(x, a, b)
        real a, b, pogr3, x
        pogr3 = a*log(x) + b
    end function pogr3

    subroutine expan(x, y, a, b, n)
        real, allocatable :: x(:), y(:)
        real a, b
        integer n

        b = exp( (sum(log(y)) * sum(x**2) - sum(log(y)*x) * sum(x)) / ( n*sum(x**2) - (sum(x))**2)  )
        a = (n*sum(log(y)*x) - sum(log(y)) * sum(x)) / (n*sum(x**2) - (sum(x))**2)
            
    end subroutine expan

    function pogr4(x, a, b)
        real a, b, pogr4, x
        pogr4 = b*exp(a*x)
    end function pogr4

    subroutine quadra(x, y, x1, n)
        real, allocatable :: x(:), y(:), A1(:,:), B1(:), X1(:)

        integer n

        allocate(A1(1:3, 1:3), B1(1:3), X1(1:3))

        A1(1,1) = sum(x**4); A1(1,2) = sum(x**3); A1(1,3) = sum(x**2);
        A1(2,1) = A1(1,2) ; A1(2,2) = A1(1,3); A1(2,3) = sum(x);
        A1(3,1) = A1(1,3); A1(3,2) = A1(2,3); A1(3,3) = n;

        !X1(1) = a; X1(2) = b; X1(3) = c
        B1(1) = sum((x**2) * y); B1(2) = sum(x*y); B1(3) = sum(y)

        call gauss(A1, B1, X1)
            
    end subroutine quadra

    function pogr5(x, a, b, c)
        real a, b, pogr5, x, c
        pogr5 = a*(x**2)  + b*x + c
    end function pogr5

    subroutine gauss(A, B, X)
        real, dimension(1:,1:) :: A
        real, dimension(1:) :: B,X
        real, allocatable :: C(:,:)
        integer i,j,ier,k
        integer n
        real R	
        
        n = size(B)
        allocate(C(n,n+1), stat = ier)
        
        if (ier /= 0) stop 1
            C(:,1:n)=A
         do concurrent (i=1:n)
            C(i,n+1) = B(i)
         enddo
            do i=1,n
                do j = i+1,n+1
                if (abs(C(i,i)) < 0.001) then
                    write(*, *) "предупреждение: деление на величину близкую к нулю"
                endif
                C(i,j) = C(i,j) / C(i,i)
                enddo		
                do k = i+1,n
                  do j = i+1, n+1
                     C(k,j) = C(k,j) - C(i,j) * C(k,i)
                  enddo
                enddo
            enddo
           
            X(n) = C(n,n+1)
            do i=n-1,1,-1
             R = 0
              do j = i+1,n
                   R = R + C(i,j) * X(j)
              enddo
             X(i) = C(i,n +1) - R  
            enddo
        end subroutine

end module mod1